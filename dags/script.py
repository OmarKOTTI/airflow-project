import requests
import pymongo
from  datetime  import  datetime


def apple():
    res_profile = requests.get("https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=743762e977b7b73e968109ce970a35bc")
    profile_apple = res_profile.json()
    res_rating = requests.get("https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=743762e977b7b73e968109ce970a35bc")
    rating_apple = res_rating.json()
    data = {"rating": rating_apple, "profile": profile_apple, 'timestamp': datetime.now()}

    conn = pymongo.MongoClient(host="kottidb", username='root', password='password')
    mydb = conn["company"]
    collection = mydb["apple"]
    id = collection.insert_one(data).inserted_id

    print(id)
    print(mydb.list_collection_names())

    #pprint.pprint(collection.find_one({"symbol": "AAPL"}))

import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from datetime import timedelta


default_args = {
    'id': 'id',
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2),
    'schedule_interval': '@daily'
}

dag = DAG(
    'project',
    default_args=default_args,
    schedule_interval=timedelta(hours=1),
)

t1 = PythonOperator(
    task_id='1',
    python_callable=apple,
    dag=dag,
)


t1 